**Returns index sizes, earlies/latest events, and other info:**
```
| rest /services/data/indexes 
| where disabled = 0 | search NOT title = "_*" 
| eval currentDBSizeGB = round( currentDBSizeMB / 1024) 
| where currentDBSizeGB > 0 
| table splunk_server title summaryHomePath_expanded minTime maxTime currentDBSizeGB totalEventCount frozenTimePeriodInSecs coldToFrozenDir maxTotalDataSizeMB 
| rename minTime AS earliest maxTime AS latest summaryHomePath_expanded AS index_path currentDBSizeGB AS index_size totalEventCount AS event_cnt frozenTimePeriodInSecs AS index_retention coldToFrozenDir AS index_path_frozen maxTotalDataSizeMB AS index_size_max title AS index
```

**Returns license usage by sourcetype/indexes:**
```
index=_internal source=*license_usage.log type="Usage" 
| eval indexname = if(len(idx)=0 OR isnull(idx),"(UNKNOWN)",idx) 
| eval sourcetypename = st 
| bin _time span=1d 
| stats sum(b) as b by _time, pool, indexname, sourcetypename 
| eval GB=round(b/1024/1024/1024, 3) 
| fields _time, indexname, sourcetypename, GB
```
