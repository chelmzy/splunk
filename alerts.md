**RDP Scanning Detected:**
```
| tstats summariesonly=t allow_old_summaries=t dc(All_Traffic.dest_ip) as target_count from datamodel=Network_Traffic where All_Traffic.dest_port=3389 All_Traffic.src_ip=10.* by All_Traffic.src_ip
| lookup rdp_scan_whitelist.csv ip_address AS All_Traffic.src_ip OUTPUT ip_address | where isnull(ip_address)
| where target_count > 25
| rename All_Traffic.src_ip as ip
```

**Short Lived Account Detected:**
```
index=<yourindex> sourcetype=XmlWinEventLog:Security user!="$*" (EventCode=4726 OR EventCode=4720) 
| eval status=case(EventCode=4726, "Account Deletion", EventCode=4720, "Account Creation") 
| transaction user startswith=status="Account Creation" endswith=status="Account Deletion" maxevents=2  
| where duration < 43200
```
